import React, { Component } from 'react';
import { LogBox, View,Text } from 'react-native'
import { StatusBar } from 'react-native';
import Navigation from "../Navigator";
// import { Root } from 'native-base'
import { Provider } from "react-redux";

import { store } from "../Store";
StatusBar.setBarStyle('light-content');
export default class App extends React.Component {
    render() {
        LogBox.ignoreLogs(['Warning: ...']); // Ignore log notification by message
        LogBox.ignoreAllLogs();//Ignore all log notifications
        return (
            <Provider store={store}>
                <Navigation />
             </Provider>
            
        );
    }
}
