import * as services from '../../Services/employee'
import Employee from '../../Data/employee'
export default {
    state: {
        loading: false,
        employees: []
    },
    reducers: {
        startLoadng(state) {
            return {
                ...state,
                loading: true,
            };
        },
        onError(state) {
            return {
                ...state,
                loading: false,
            };
        },
        onSuccessEmployeeList(state, data) {
            return {
                ...state,
                employees: data,
                loading: false,
            };
        }

    },
    effects: {
        async getEmployee() {
            try {
                this.startLoadng()
                let res = await Employee.getContent()
                if (res) {
                    await this.onSuccessEmployeeList(res)
                } else {
                    this.startLoadng()
                    res = await services.getEmployees()
                    await this.onSuccessEmployeeList(res)
                    await Employee.setContent(res)
                }
                return true

            } catch (e) {
                throw e
            }
        }
    }
};
