import React from "react";
import {
  createAppContainer, createSwitchNavigator,
} from "react-navigation";
import {
  createStackNavigator
} from "react-navigation-stack"

import Employee from "../Modules/Employee";
import EmployeeDetail from "../Components/EmployeeDetail";
const AppNavigator = createSwitchNavigator(
  {
    // Splash: { screen: SplashScreen },
    Splash: {
      screen: createStackNavigator(
        {
            Employee: Employee,
            EmployeeDetail:EmployeeDetail
        },
        {
          headerMode: "none",
          initialRouteName: 'Employee'
        },
      )
    },
    // Home: { screen: Employee },
  }
);
export default createAppContainer(AppNavigator);