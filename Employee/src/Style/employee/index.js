import { StyleSheet } from 'react-native'
export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        margin: 0,
        padding: 15
    },
    welcomeText: {
        fontSize: 18,
        color: 'white',
        fontWeight: 'bold',
        marginBottom:15,
        //   letterSpacing:1
    },
    nameText:{
        fontSize:16,
        margin:20
    }
})