import data from "../index"
export default class Employee {
    static async setContent(value) {
        if (value) {
            let response = JSON.stringify(value ? value : {})
            await data.set("empData",response)

        }
    }
    static async getContent() {
        try {
            let response = await data.get("empData");
            var jsonResponse = JSON.parse(response ? response : {});
            return jsonResponse 
        } catch (err) {
            return "";
        }
    }
    static async clearContent() {
        try {
            await data.set("empData", "")
        } catch (err) {
            return false;
        }
    }
}
