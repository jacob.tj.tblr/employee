import {  get } from "../Api";

import {baseUrl,api} from '../../Config/Server'
export async function getEmployees() {
    return await get(`${baseUrl.url}${api.listEmployees}`);
  }