import React from "react";
import { Text, View, SafeAreaView, TouchableOpacity, FlatList, Image, TextInput } from 'react-native'
import { styles } from "../../Style/employee";
export default class EmployeeList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            searchText: ''
        };
    };
    renderItemView(item, index) {

        return (
            <View >
                {/* <Image style={{ width: 8, height: 8, resizeMode: 'contain',tintColor:'white' }} source={require(item.profile_image)} /> */}
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate('EmployeeDetail', { item })
                    }}>

                    <View style={{ backgroundColor: '#deebff', flexDirection: 'row', margin: 10, width: '100%' }}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Image
                                style={{ width: 60, height: 60, borderRadius: 30 }}
                                source={{ uri: item.profile_image }}
                                resizeMode={'contain'} // cover or contain its upto you view look
                            />
                        </View>
                        <View style={{ flexDirection: 'column', flex: 4 }}>
                            <Text style={styles.nameText}>{item.name}</Text>
                            <Text style={styles.nameText}>{item.company && item.company && item.company.name}</Text></View>
                    </View>

                </TouchableOpacity>


            </View>
        )
    }
    async search() {
        let item = {}
        for (let i = 0; i < this.props.employees.length; i++) {
            if (this.props.employees[i].name == this.state.searchText) {
                console.log(this.props.employees[i], "search");
                item = await this.props.employees[i]
                await this.props.navigation.navigate('EmployeeDetail', { item })
            }


        }
        if (temp == 1) {
            alert("No user Found")
        }

    }
    render() {
        return (
            <View style={{ flex: 1 }}>

                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ flexDirection: 'row',height:35 }}>
                        <TextInput
                            name="search"
                            style={{ marginLeft: 20, flex: 9 }}
                            placeholder={"Search"}
                            placeholderTextColor="#808080"
                            onChangeText={search => this.setState({ searchText: search })}

                        />
                        <TouchableOpacity onPress={() => {
                            this.search()
                        }}>
                            <View style={{ flex: 3, backgroundColor: '#4d88e8', width: 70, height: 35, borderRadius: 5, alignItems: 'center', justifyContent: 'center', alignSelf: 'flex-end', marginRight: 10 }}>
                                <Text style={{ fontSize: 14, color: 'white' }}>Search</Text></View></TouchableOpacity>
                    </View>
                    <FlatList
                        style={{ flex: 1 }}
                        data={this.props.employees}
                        renderItem={({ item, index }) =>
                            this.renderItemView(item, index)
                        }
                        // onEndReached={() => {
                        //   this.props.onEnd()
                        // }}
                        // numColumns={3}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={(item, index) => item + index}
                    // onEndReachedThreshold={1}
                    />
                </SafeAreaView>
            </View>
        )
    }
}