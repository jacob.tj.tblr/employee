import React from "react";
import { Text, View, SafeAreaView, TouchableOpacity, FlatList, Image } from 'react-native'
import { styles } from "../../Style/employee";
export default class EmployeeList extends React.Component {
    details(item) {
        return (
            <View style={{ alignItems: 'flex-start', }}>
                <Text style={styles.nameText}>{"Name : " + item.name}</Text>
                <Text style={styles.nameText}>{"Email :" + item.email}</Text>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{justifyContent:'center',flex:1.5}}>
                        <Text style={styles.nameText}>{"Address :"}</Text>
                    </View>
                    <View style={{ flexDirection: 'column',flex:3 }}>
                        <Text style={[styles.nameText],{marginTop:5}}>{item.address.city}</Text>
                        <Text style={[styles.nameText],{marginTop:5}}>{item.address.street}</Text>
                        <Text style={[styles.nameText],{marginTop:5}}>{item.address.suite}</Text>
                        <Text style={[styles.nameText],{marginTop:5}}>{item.address.zipcode}</Text>
                    </View>
                </View>
                <Text style={styles.nameText}>{"Phone :"+ item.phone}</Text>
                <Text style={styles.nameText}>{"Website :"+ item.website}</Text>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{justifyContent:'center',flex:1.5}}>
                        <Text style={styles.nameText}>{"Company :"}</Text>
                    </View>
                    <View style={{ flexDirection: 'column',flex:3 }}>
                        <Text style={[styles.nameText],{marginTop:5}}>{item.company.name}</Text>
                        <Text style={[styles.nameText],{marginTop:5}}>{item.company.catchPhrase}</Text>
                        <Text style={[styles.nameText],{marginTop:5}}>{item.company.bs}</Text>
                    </View>
                </View>
            </View>
        )
    }
    render() {
        let item = this.props.navigation.state.params.item
        console.log(this.props.navigation.state,"ute,");
        return (
            <View style={styles.container}>
                <SafeAreaView style={{ height: '100%', width: '100%', alignItems: 'center' }}>
                    <View style={{ backgroundColor: '#deebff', margin: 10, height: '100%', width: '100%', }}>
                        <Image
                            style={{ width: 120, height: 120, borderRadius: 60, marginTop: 20, alignSelf: 'center' }}
                            source={{ uri: item.profile_image }}
                            resizeMode={'contain'} // cover or contain its upto you view look
                        />

                        {this.details(item)}
                    </View>
                </SafeAreaView>
            </View>
        )
    }
}