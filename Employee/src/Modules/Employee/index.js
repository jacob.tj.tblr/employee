import React,{Component} from "react";
import { View,Text } from "react-native";
import {styles} from "../../Style/employee";
import Emp from "../../Data/employee";
import EmployeeList from "../../Components/EmployeeList"
import { connect } from "react-redux";
import { compose } from "recompose";
const mapDispatchToProps = ({ employeeModal }) => {
    return {
      ...employeeModal
    };
  };
  
  const mapStateToProps = ({ employeeModal }) => {
    return {
      ...employeeModal
    };
  };
class Employee extends React.Component{
    async componentDidMount(){
        await this.props.getEmployee();

    }
    render(){
        return(
          <View style={{ flex: 1, backgroundColor: '#F5F5F5' }}>
                <View style={{width:'100%',height:'100%'}}>
                    <EmployeeList employees={this.props.employees} navigation={this.props.navigation}/>
                </View>
            </View>
        )
    }
}
export default compose(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )
  )(Employee);